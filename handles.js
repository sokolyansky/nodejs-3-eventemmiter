'use strict';

let chatOnMessage = (message) => {
  console.log(message);
};

let prepareToAnswer = () => {
  console.log('Готовлюсь к ответу...');
};

module.exports = {
  chatOnMessage,
  prepareToAnswer
};