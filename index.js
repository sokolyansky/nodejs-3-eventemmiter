'use strict';
const ChatApp = require('./app');
const {chatOnMessage, prepareToAnswer} = require('./handles');

let webinarChat =  new ChatApp('webinar');
let facebookChat = new ChatApp('=========facebook');
let vkChat =       new ChatApp('---------vk');


webinarChat.on('message', chatOnMessage);
facebookChat.on('message', chatOnMessage);
vkChat.on('message', chatOnMessage);


// Закрыть вконтакте
setTimeout( ()=> {
  console.log('Закрываю вконтакте...');
  vkChat.removeListener('message', chatOnMessage);
}, 10000 );


// Закрыть фейсбук
setTimeout( ()=> {
  console.log('Закрываю фейсбук, все внимание — вебинару!');
  facebookChat.removeListener('message', chatOnMessage);
}, 15000 );



webinarChat.on('message', prepareToAnswer);
vkChat.setMaxListeners(2);
vkChat.on('message', prepareToAnswer);
vkChat.on('close', () => {
  console.log('Чат вконтакте закрылся :(');
});

setTimeout( () => {
  vkChat.close();
}, 1000 * 2);

setTimeout( () => {
  webinarChat.removeListener('message', chatOnMessage);
}, 1000 * 30);